import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-home',
  styleUrl: 'app-home.css',
  shadow: true
})
export class AppHome {

  render() {
    return (
      <div class='app-home'>
        <p>
          Welcome to the Redyno App Starter.
          You can use this starter to build entire apps all with
          web components using Stencil!
        </p>

        <stencil-route-link url='https://redyno.stoplight.io'>
          <button>
            Open redyno homepage
          </button>
        </stencil-route-link>
      </div>
    );
  }
}
